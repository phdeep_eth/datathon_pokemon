#!/usr/bin/env python3

# Poke' Hackathon
# Luca & Francesco, ETH Zurich
# 19 May 20 - 31 May 20

import pandas as pd
import numpy as np

def get_matrix_with_features(potential_battles, weak_poke, all_poke):
    """ augment pokemons battles matrix with additional features """
    # drop the duplicate pokemons
    allPoke_correct = all_poke.drop_duplicates()
    
    # split the dataframe of the battles by the two pokemon
    pk1_battle = potential_battles[['Name_1', 'Level_1', 'Price_1', 'HP_1', 'Attack_1', 'Defense_1',
                                 'Sp_Atk_1', 'Sp_Def_1', 'Speed_1', 'Legendary_1', 'Name_2']]
    pk2_battle = potential_battles[['Name_2', 'Level_2', 'Price_2', 'HP_2', 'Attack_2', 'Defense_2',
                                 'Sp_Atk_2', 'Sp_Def_2', 'Speed_2', 'Legendary_2']]
    
    # add weaknesses and strengths of pokemon 1 and pokemon 2 involved in the battle
    pk1_battle_addF = pk1_battle.merge(allPoke_correct, how ='left', left_on='Name_1', right_on='Name').drop(columns=['Name'])
    pk1_battle_addF = pk1_battle_addF.merge(allPoke_correct, how ='left', left_on='Name_2', right_on='Name').drop(columns=['Name'])
    pk1_battle_addF = pk1_battle_addF.drop(columns=['Name_2'])
    
    # copy features pokemon 2
    pk2_battle_addF = pk2_battle.copy()
    
    # add the column features
    pk1_battle_addF["pk1_feat_1"] = np.nan
    pk1_battle_addF["pk1_feat_2"] = np.nan
    pk1_battle_addF["pk1_feat_3"] = np.nan
    pk1_battle_addF["pk1_feat_4"] = np.nan
    
    pk2_battle_addF["pk2_feat_1"] = np.nan
    pk2_battle_addF["pk2_feat_2"] = np.nan
    pk2_battle_addF["pk2_feat_3"] = np.nan
    pk2_battle_addF["pk2_feat_4"] = np.nan
    
    # concatenate the two pokemon matrices
    result = pd.concat([pk1_battle_addF, pk2_battle_addF], axis=1)  
    
    # define the index of the types
    pk1_type1_idx = result.columns.get_loc('Type_1_x')
    pk1_type2_idx = result.columns.get_loc('Type_2_x')
    pk2_type1_idx = result.columns.get_loc('Type_1_y')
    pk2_type2_idx = result.columns.get_loc('Type_2_y')
    # the +1 is beacause row include the index
    for row in result.itertuples():
        result.at[row.Index, 'pk1_feat_1'] = weak_poke[ weak_poke['Types'] == row[pk1_type1_idx+1] ][ row[pk2_type1_idx+1] ]

        result.at[row.Index, 'pk1_feat_2'] = np.nan if pd.isnull([row[pk2_type2_idx+1]]) else weak_poke[weak_poke['Types'] == row[pk1_type1_idx+1]][row[pk2_type2_idx+1]]
         
        result.at[row.Index, 'pk1_feat_3'] =  np.nan if pd.isnull([row[pk1_type2_idx+1]]) else weak_poke[weak_poke['Types'] == row[pk1_type2_idx+1]][row[pk2_type1_idx+1]]

        result.at[row.Index, 'pk1_feat_4'] =  np.nan if (pd.isnull([row[pk1_type2_idx+1]]) or pd.isnull([row[pk2_type2_idx+1]])) else weak_poke[weak_poke['Types'] == row[pk1_type2_idx+1]][row[pk2_type2_idx+1]]

        result.at[row.Index, 'pk2_feat_1'] = weak_poke [ weak_poke['Types'] == row[pk2_type1_idx+1] ][ row[pk1_type1_idx+1] ]

        result.at[row.Index, 'pk2_feat_2'] = np.nan if pd.isnull([row[pk1_type2_idx+1]]) else weak_poke[weak_poke['Types'] == row[pk2_type1_idx+1]][row[pk1_type2_idx+1]]

        result.at[row.Index, 'pk2_feat_3'] = np.nan if pd.isnull([row[pk2_type2_idx+1]]) else weak_poke[weak_poke['Types'] == row[pk2_type2_idx+1]][row[pk1_type1_idx+1]]

        result.at[row.Index, 'pk2_feat_4'] = np.nan if (pd.isnull([row[pk1_type2_idx+1]]) or pd.isnull([row[pk2_type2_idx+1]])) else weak_poke[weak_poke['Types'] == row[pk2_type2_idx+1]][row[pk1_type2_idx+1]]
        
    return result


def clamp_prediction(X_test, predictions_scaled):
    """ clamp the health points left after a battle to the initial health points of the two pokemons """
    min_values = -np.array(X_test['HP_2']).reshape(-1,1)
    max_values = np.array(X_test['HP_1']).reshape(-1,1)
    check = (predictions_scaled>max_values) | (predictions_scaled<min_values)
    
    X_clamp = X_test[['HP_1']][check]
    X_clamp['HP_2_inv'] = -X_test[['HP_2']][check]
    X_clamp['Pred2clamp'] = predictions_scaled[check]
    # find on which side we have to clamp
    X_clamp['diff_1'] = X_clamp['Pred2clamp'] - X_clamp['HP_1']
    X_clamp['diff_2'] = X_clamp['HP_2_inv'] - X_clamp['Pred2clamp']
    X_clamp['test'] = X_clamp['diff_1'] < X_clamp['diff_2']
    # clam the prediction
    X_clamp['PredClamped'] = X_clamp['HP_1']
    X_clamp['PredClamped'][X_clamp['test']] = X_clamp['HP_2_inv']
    
    predictions_scaled[check] = X_clamp['PredClamped']
    return predictions_scaled

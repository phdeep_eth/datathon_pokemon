# PokéHackathon
**Dataset**:   database of about 2.7 Million hystorical Pokémon battles. Each Pokémon belongs to one or two specific types (e.g. water, fire, poison, ... ) and the strength of one type agains another is known. Each Pokémon is characterized by 10 extra features like health point (HP), level, attack and defence strength, and speed. Depending on these features, the Pokémon has a certain price.\
**Challenge**: given a list of 1.4 K available Pokémon, we were called to select six of them to fight against the given Pokémon opponents in the legendary Pokéleague.\
**Solution**: we present our solution in the following 3 slides. 

Authors: *L. Pedrelli*, *F. Saltarelli*\
Last update: 23. June 2020

# Folder structure

| Folder name               | Description   |
| :---                      | :---          |
| data                      | raw data given for the PokéHackathon and challange description | 
| weights_PokeNet           | weights of the fully connected neural network developed for for battle prediction |
| presentation              | final presentation and slide images | 
| *.pickle                  | pickle files containing pre-processed data and additional features extracted through feature engineering |

# Python scripts

*Data_preprocessing.ipynb*:
- Convert the csv files to pickle.
- Add Pokémons strengths and weaknesses to the batte_result matrix and to the potential battles.
- Create a matrix with all the possible battles for the PokéLeague.

*Poke_ML_model.ipynb*: 
- Load the data from the created pickle files.
- Preprocess them (standard scaler and optional left-out set used only to test performance).
- Define the neural network model PokéNet and train it on differed folds of battle_result.
- Measure the performance of the network on the left-out set (if used).
- Select the best team according to the efficient frontier and the sharpe ratio.

*function_import.py* (helper functions):
- Function to add Pokémons strengths and weaknesses to the battles.
- Clamps the health points left after a battle between –HP2 and HP1, where HP1 and HP2 are the initial health points of the contenders.

# Presentation slides

![Step1 slide](presentation/Slide2.png)
![Step2 slide](presentation/Slide3.png)
![Step3 slide](presentation/Slide4.png)

# Disclaimer
The data and the script we developed are open source.\
The organizer allowed us to share our work and the dataset of the PokéHackathon.\